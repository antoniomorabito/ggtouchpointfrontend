const ButtonPrimary = ({ text, onClick, active }) => {
    return (
        <button
            style={{
                background: active && '#ECD898' || 'gray',
                borderRadius: 10,
                color: active && 'black' || 'white',
                width: '100%',
                border: 0,
                height: 40,
                fontSize: 16,
                cursor: 'pointer'
            }}
            onClick={() => onClick != null && onClick()}>
                {text}
        </button>
    )
}

ButtonPrimary.defaultProps = {
    active: true,
    onClick: null 
}

export default ButtonPrimary
