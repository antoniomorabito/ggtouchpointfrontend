import React from 'react'
import { useHistory } from 'react-router-dom'
import { isSafari } from 'react-device-detect'
import { FaArrowLeft, FaAward, FaSearch, FaStar } from 'react-icons/fa'
import './navbar-search-menu.css'

export const NavbarSearchWithMenu = ({placeholder}) => {

    const history = useHistory()
    
    const onBackClicked = () => {
        if (isSafari) {
            history.go(-2)
        } else {
            history.goBack()
        }
    }

    return (
        <div className="navbar-search-menu-container">
            <div className="navbar-search-menu-container-2">
                <div className="navbar-search-menu-container-3">
                    <div className="navbar-search-menu-input-container">
                        <FaSearch className="navbar-search-menu-icon"/>
                        <input className="navbar-search-menu-input"
                            placeholder={placeholder}/>
                    </div>
                    <a style={{background: 'none', zIndex: 10}} href='#'>
                        <FaArrowLeft className="navbar-search-menu-back-icon"
                            onClick={() => onBackClicked() }/>
                    </a>
                </div>
            </div>
            <div className="navbar-search-menu-container-4">
                <div className="navbar-search-menu-item-container">
                    <FaStar className="navbar-search-menu-item-container"/>
                </div>
                <div className="navbar-search-menu-item-container">
                    <FaAward className="navbar-search-menu-item-container"/>
                </div>
            </div>
        </div>
    )
}

export default NavbarSearchWithMenu