import { message } from 'antd'
import React from 'react'
import { FaPlus } from 'react-icons/fa'
import GalleryMenuVenueRow from '../../../components/galleryvenuerow/GalleryMenuVenueRow'
import { ERROR, getJsonResponse, ggAlert, headerNoUrlEncodedBearer, url } from '../../../service/Service'
import './gallery-venue.css'

const GalleryVenue = ({data, onDelete}) => {

    async function deleteData(e) {
        var formData = new FormData()
        
        formData.append("id_gallery", e.id)

        const requestOptions = {
            method: 'POST',
            body: formData,
            headers: headerNoUrlEncodedBearer
        }

        await fetch(url + "horeca/venue/galleryhoreca/delete", requestOptions)
            .then(response => getJsonResponse(response))
            .then(json => {
                onDelete()
            })
            .catch(error => ggAlert("Error", error, ERROR))
    }

    return (
        <div className="gallery-venue-container">
            <div className="gallery-venue-container-2">
                {
                    data.map(function(d) {
                        return <GalleryMenuVenueRow
                            data={d}
                            deleteData={deleteData}/>
                    })
                }
            </div>
            <FaPlus className="gallery-venue-add"/>
        </div>
    )
}

export default GalleryVenue
