import React, { useState } from 'react'
import ButtonPrimary from '../../components/ButtonPrimary'
import NavbarDetail from '../../components/navbardetail/NavbarDetail'
import UserType from '../../components/usertype/UserType'
import './register.css'
import { Link } from 'react-router-dom'

const Register = () => {

    const [isCustomerShown, setCustomerShown] = useState(false)
    const [isMerchantShown, setMerchantShown] = useState(false)

    const shownCustomer = (isShown) => {
        setCustomerShown(isShown)
        if (isMerchantShown && isShown) {
            setMerchantShown(false)
        }
    }

    const shownMerchant = (isShown) => {
        setMerchantShown(isShown)
        if (isCustomerShown && isShown) {
            setCustomerShown(false)
        }
    }

    return (
        <div className="parent">
            <NavbarDetail
                title="DAFTAR"/>
            <div className="container">
                <div className="registerUserTypeContainer">
                    <UserType
                        gap={20}
                        isCustomer={true}
                        isShown={isCustomerShown}
                        clicked={shownCustomer}/>
                    <p className="registerLabel">or</p>
                    <UserType
                        isShown={isMerchantShown}
                        clicked={shownMerchant}/>
                </div>
                <div className="fillSpaceContainer"
                    style={{height: 'calc(100% - 440px)'}}>
                    <Link to="/verification">
                        <ButtonPrimary
                            text="Register"/>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Register
