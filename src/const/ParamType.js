export const TYPE_HORECA_LIST = "horeca-list"
export const TYPE_HORECA_LIST_PROMO_RELATED = "horeca-list_promo_related"
export const TYPE_HORECA_LIST_REWARD_RELATED = "horeca-reward-related"
export const TYPE_EDIT = "edit"
export const TYPE_STORE_FROM_REWARD = "reward"
export const TYPE_DETAIL = "detail"